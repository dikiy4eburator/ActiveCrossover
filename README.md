# 3-Way Active Crossover
scematic and pcb for a 24dB/Oct active crossover based on the
"State Variable filter".
    
    - crossover points:
          - lowpass <-> bandpass: variabel, 66 - 408 Hz
          - bandpass <-> highpass: variabel, 660 - 4080 Hz
    
    - IC: 9x LM4562 / OPA2134 / TL072  OpAmp
          1x INA134
          3x DRV134 Line driver

    - power supply max +/- 15V DC


prototype is working.


<img src="active_crossover_front.png"
     style="float: left; margin-right: 10px;" />

<img src="active_crossover_naked_front.png"
     style="float: left; margin-right: 10px;" />

<img src="active_crossover_naked_back.png"
     style="float: left; margin-right: 10px;" />
